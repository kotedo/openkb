var tinyMCETemplateList = [
    ["Handover-Ticket", "/static/templates/handover.html"],
    ["Intensive Managed Linux Spheres of Support", "/static/templates/intemanlinsos.html"],
    ["RackConnect", "/static/templates/rackconnect.html"],
    ["Rackspace Corporate Customer Runbook", "/static/templates/corporaterunbook.html"],
    ["Rackspace Customer Runbook","/static/templates/customerrunbook.html"],
    ["Rackspace Generic", "/static/templates/generic.html"],
    ["Rackspace Goal", "/static/templates/goal.html"],
    ["Problem + Solution","/static/templates/problemsolution.html"],
    ["Rackspace System", "/static/templates/rackspacesystem.html"],
    ["cas/Audit", "/static/templates/casaudit.html"],
    ["eBay Maintenance", "/static/templates/ebaymaintenance.html"]
];
