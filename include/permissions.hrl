%%%-------------------------------------------------------------------
%%% @author Kai Janson <kai.janson@MHM23TDV13>
%%% @copyright (C) 2012, Kai Janson
%%% @doc
%%%
%%% @end
%%% Created : 20 Jul 2012 by Kai Janson <kai.janson@MHM23TDV13>
%%%-------------------------------------------------------------------
-define(ADMIN,[create, read, update, delete, admin_users, admin_site]).
-define(POWERUSER,[create, read, update, delete]).
-define(USER,[read]).
