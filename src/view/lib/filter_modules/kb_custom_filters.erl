-module(kb_custom_filters).
-compile(export_all).

% put custom filters in here, e.g.
%
% my_reverse(Value) ->
%     lists:reverse(binary_to_list(Value)).
%
% "foo"|my_reverse   => "foo"

can(User, Permission) ->
    Role = User:role(),
    lists:member(Permission, Role:permissions()) == true.

localdate(Timestamp) ->
    utils:tohumandatetime(Timestamp).

shortdate([undefined]) ->    
    "no date set";
shortdate(undefined) ->
    "no date set";
shortdate([]) ->
    "no date set";
shortdate( Value ) when is_binary(Value) ->
    V = binary_to_list(Value),
    Result = case re:run(V, "(\\d{1,2})/(\\d{1,2})/(\\d{2,4})", [{capture, all_but_first, list}]) of
		  {match, [Month, Day, Year]} ->
		          lists:flatten(io_lib:format("~s/~s", [Month, Day]));
		  nomatch ->
		          V;
		  StuffWentCrazy ->
		          V
			      end,
    Result.
