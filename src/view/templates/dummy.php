<?php // this must be the very first line in your PHP file!

// You can't simply echo everything right away because we need to set some headers first!
$output = ''; // Here we buffer the JavaScript code we want to send to the browser.
$delimiter = "\n"; // for eye candy... code gets new lines

$output .= 'var tinyMCEImageList = new Array(';

// $directory = "../../media"; // Use your correct (relative!) path here
// $directory = "src/view/templates"; // Use your correct (relative!) path here
$directory = "."; // Use your correct (relative!) path here

// echo("Directory: $directory\n");

// allowed extensions
$TinyMceMediaExts = array(	'tpl'=> '1',
							'tmpl' => '1',
							'htm' => '1',
							'html' => '1'
						);

// Since TinyMCE3.x you need absolute image paths in the list...
$abspath = preg_replace('~^/?(.*)/[^/]+$~', '/$1', $_SERVER['SCRIPT_NAME']);

if (is_dir($directory)) {
    $direc = opendir($directory);
    while ($file = readdir($direc)) {
		// echo("dir/file: $directory/$file\n");
     if (!preg_match('^.~', $file)) { // no hidden files / directories here...
             if (is_file("$directory/$file")) {
                // We got ourselves a file! Make an array entry:

                preg_match('/\.([^.]+)$/',$file,$match);
				// echo("match: $match");
                if($TinyMceMediaExts[@$match[1]]){
                    $output .= $delimiter
                               . '["'
                               . utf8_encode($file)
                               . '", "'
                               . utf8_encode("$abspath/$directory/$file")
                               . '"],';
               } else { echo("No match\n"); }
            } else { echo("Not a file '$file'\n"); }
        } else { echo("No match love '$directory/$file' \n"); }
    }

    $output = substr($output, 0, -1); // remove last comma from array item list (breaks some browsers)
    $output .= $delimiter;

    closedir($direc);
}

// Finish code: end of array definition. Now we have the JavaScript code ready!
$output .= ');';

// Make output a real JavaScript file!
header('Content-type: text/javascript'); // browser will now recognize the file as a valid JS file

// prevent browser from caching
header('pragma: no-cache');
header('expires: 0'); // i.e. contents have already expired

// Now we can send data to the browser because all headers have been set!
echo($output);

?>
