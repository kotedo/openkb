-module(author, [Id, Sso, First, Last, Email, Active, RoleId, CreatedOn, UpdatedOn]).
-compile(export_all).

-belongs_to(role).

-has({category_articles, many}).
-has({articles, many}).
-has({author_articles, many}).
-has({tag_articles, many}).
-has({ranking, 1}).
-has({author_profile, 1}).

%% For historical data
-has({article_histories,many}).

fullname() ->
    string:concat(binary_to_list(First), [" ", binary_to_list(Last)]).
