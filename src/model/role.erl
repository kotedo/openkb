-module(role, [Id, Name, DisplayName, Permissions]).
-compile(export_all).
-include("permissions.hrl").

-has({authors, many}).
