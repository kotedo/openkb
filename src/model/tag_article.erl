-module(tag_article, [Id, ArticleId, AuthorId, Tag, Title]).
-compile(export_all).

-belongs_to(article).
-belongs_to(author).
