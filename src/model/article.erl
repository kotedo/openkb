-module(article, [Id, Title, Content, CategoryId, Tags, AuthorId, ArticleRatingId, CreatedOn, UpdatedOn, Status]).
-compile(export_all).

-belongs_to(category).
-belongs_to(author).
-belongs_to(article_rating).

-has({author_articles, many}).
-has({tag_articles, many}).
-has({category_articles, many}).
