-module(author_article, [Id, AuthorId, ArticleId, Title]).
-compile(export_all).

-belongs_to(author).
-belongs_to(article).
