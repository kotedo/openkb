-module(ranking, [Id, AuthorId, BadgeId, Rank, RankName]).
-compile(export_all).

-belongs_to(author).
-belongs_to(badge).
