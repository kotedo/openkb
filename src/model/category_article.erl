-module(category_article, [Id, CategoryId, ArticleId, AuthorId, Title]).
-compile(export_all).

-belongs_to(category).
-belongs_to(article).
-belongs_to(author).
