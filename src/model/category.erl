-module(category, [Id, Name]).
-compile(export_all).

-has({articles, many}).
-has({category_articles, many}).
-has({article_histories, many}).

before_create() ->
    case boss_db:find(category, [{name, THIS:name()}]) of
	[_Data] ->
	    {error, lists:flatten(io_lib:format("Category ~s  already exists. Not created", [THIS:name()]))};
	[] ->
	    error_logger:info_msg("Category ~p wasn't found, so we can create it.", [THIS:name()]),
	    ok
    end.
