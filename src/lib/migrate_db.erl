-module(migrate_db).
-export([up/0, down/0,up/1,down/1]).
-export([display/1]).
-export([work/1]).
-export([execute/3]).

-spec up() -> ok | {error, Reason::string()}.
up() ->
    {ok, Migrations} = file:consult("migrations"),
    error_logger:info_msg("Migrations: ~p~n", [Migrations]),

    work_on_blocks(Migrations),

    %%UpMigrations = proplists:get_all_values(up, [Migration || Migration <- Migrations]),
    
    %%Amount = length(UpMigrations),
    %%io:format("Migrations: ~b~nMigrations: ~p~n~n~b UpMigrations loaded:~n~p~n~n", [length(Migrations), Migrations, Amount, UpMigrations]),

    %% display(UpMigrations),
    %%work(UpMigrations),
    ok.

-spec up(Version::string()) -> ok | {error, Reason::string()}.
up(Version) ->
    ok.

-spec down() -> ok | {error, Reason::string()}.
down() ->
    ok.

-spec down(Version::string()) -> ok | {error, Reason::string()}.
down(Version) ->
    ok.

%%display([]) ->
%%    ok;
display([Head | MigrationsList]) ->
    io:format("Migration: ~p~n~n", [Head]),
    display(MigrationsList).

work_on_blocks([]) ->
    ok;
work_on_blocks([Head|Block]) ->
    %% work(hd(Head)),
    error_logger:info_msg("~n~nHead of block is: ~p~n~n", [Head]),
    work_on_blocks(Block).


%%work([]) ->
%%    ok;
work([Workorder|MigrationsList]) ->
    error_logger:info_msg("Working on ~p~n~n", [Workorder]),

    Action = proplists:get_value(action, Workorder),
    Table = proplists:get_value(table, Workorder),
    
    error_logger:info_msg("Work on table ~p and perform ~p.~n", [Table, Action]),
    execute(Action, Table, Workorder),
    work(MigrationsList).

execute(rename, Table, Options) ->
    Column = proplists:get_value(column, Options),
    NewName = proplists:get_value(new_name, Options),
    error_logger:info_msg("Renaming column ~p of table ~p to ~p.~n", [Column, Table, NewName]);
execute(create_index, Table, Options) ->
    IndexName = proplists:get_value(index_name, Options),
    case proplists:get_value(unique, Options) of
	true ->
	    error_logger:info_msg("Creating unique index ~p on table ~p.~n", [IndexName, Table]);
	false ->
	    error_logger:info_msg("Creating index ~p on table ~p.~n", [IndexName, Table])
    end;
execute(add_row, Table, Options) ->
    ok;
execute(UnknownAction, Table, _) ->
    error_logger:error_msg("Don't know how to perform action ~p on table ~p~n~n", [UnknownAction, Table]).
