-module(seed_db).
-compile(export_all).

-include("permissions.hrl").


create_roles() ->
    [(role:new(id, Name, DisplayName, Permissions)):save() || {Name, DisplayName, Permissions} <- [
									  {"admin","Administrator",?ADMIN},
									  {"poweruser","Power User",?POWERUSER},
									  {"user","User", ?USER}]].
