-module(search).
-export([casei/1, see_also/2, simple/1]).

%%-spec casei (Value::string()) -> {ok, CaseInsensitive::string()} || {error, undefined}.
casei([]) ->
    <<"">>;
casei(List) ->
    error_logger:info_msg("~n~nIncoming search term(s): ~p~n~n", [List]),
    lists:flatten(lists:foldr(fun(X,Result) -> [ "[",string:to_upper(X),string:to_lower(X),"]" |Result] end, [], List)).

see_also(ThisId, Tags) ->
    Articles = [(boss_db:find(tag_article, [{tag, Tag}, {id, 'not_equals', ThisId}])):id() || Tag <- [string:strip(T, both)|| T <- string:tokens(Tags, ",")]],
    ok.

simple(SearchString) ->
    error_logger:info_msg("search:simple(~p) underway~n", [SearchString]),
    [
     %% [Id, Title, Content, CategoryId, Tags, AuthorId, ArticleRatingId, CreatedOn, UpdatedOn, Status]).
     {by_tags, boss_db:find(article, [{tags, 'matches', string:to_lower("*"++SearchString)}])},
     %% {by_categories, boss_db:find(category_article, [{name, 'matches', SearchString}])},
     {by_titles, boss_db:find(article, [{title, 'matches', "*"++SearchString}])},
     {by_content, boss_db:find(article, [{content, 'matches', "*"++SearchString}])}
    ].
