-module(security).
-compile(export_all).

li(SessionID) ->
    case boss_session:get_session_data(SessionID, fullname) of
	undefined ->
	    false;
	Fullname ->
	    Fullname
    end.
