-module(utils).
-export([timestamp/0,
	 todatetime/1,
	 tohumandatetime/1,
	 make_tags/1
	]).

timestamp() ->
    calendar:datetime_to_gregorian_seconds(calendar:now_to_datetime(now())).

todatetime(GregorianSeconds) ->
    calendar:universal_time_to_local_time(calendar:gregorian_seconds_to_datetime(GregorianSeconds)).

tohumandatetime(GregorianSeconds) ->
    {{Year, Month, Day}, {Hour, Minute, Second}} = todatetime(GregorianSeconds),
    lists:flatten(io_lib:format("~2..0w/~2..0w/~4..0w ~2..0w:~2..0w:~2..0w", [Month, Day, Year, Hour, Minute, Second])).

%% Possible versions of TAGS
%% A = "ajsdhasda asdasdasdas asdsad asd".
%% B = "fdsfdsfs,ds fsddsfdsf,sdfsdfsdf,sdfsdf sf sdf s,ffsdfsf".
%% C = ["adad", "asda", "asda"].
%% D = ["dfsadasd asdasdas","sdfdsfs asdad"].
%%
%% make_tags(Tags) ->
%%     case string:tokens(Tags, " ") andalso string:tokens(Tags, ",") of
%% 	true ->
%% 	    %% It's B
%% 	    %% Stuff
%% 	    ok;
%% 	false ->
%% 	    %% Is is A?
%% 	    case string:tokens(Tags, " ") 

make_tags(Tags) when is_binary(Tags) ->
    error_logger:info_msg("Tags: (Binary): '~p'~n", [Tags]),
    make_tags(binary_to_list(Tags));
make_tags(Tags) ->
    T = case string:str(Tags, ",") of
	    0 ->
		error_logger:info_msg("Tags: (0): '~p'~n", [Tags]),
		string:tokens(Tags, " ");
	    _ ->
		error_logger:info_msg("Tags: (_): '~p'~n", [Tags]),
		string:tokens([Tags], ",")
	end,
    T.


