%%%----------------------------------------------------------------------------
%%% @author Kai Janson <kai.janson@rackspace.com
%%% [http://www.rackspace.com]
%%% @copyright 2012 Kai Janson / Rackspace
%%% @doc Selenium Tests for the landing page
%%% @end
%%%----------------------------------------------------------------------------                                                                                
-module(landing_page_tests).
-include_lib("eunit/include/eunit.hrl").

-export([landing_page/2]).
%% -export([setup/0]).
%% -export([setup_session/1]).

suite_test_() ->
    [{setup,
      fun() ->
	      setup_session(B) end,
      fun close_session/1,
      api_tests(B)} || B <- cb_sel:get_browsers()].

api_tests(Browser) ->
    Tests = [
	     landing_page
	    ],
    fun(X) ->
	    [{timeout, 120,
	      {lists:flatten(io_lib:format("~s with ~s", [T, Browser])), fun() ->
										 ?MODULE:T(Browser,X) end}
	      } || T <- Tests]
    end.

setup() ->
    ok.

setup_session(Browser) ->
    {ok, Session} = webdriver_remote:session(cb_sel:get_host(),
					     cb_sel:get_port(),
					     [
					      {browserName, Browser},
					      {javascriptEnabled, true},
					      {version, <<"">>},
					      {platform, 'ANY'}
					     ]
					    ),
    Session.

close_session(Session) ->
    {ok, no_content} = webdriver_remote:quit(Session).

landing_page(_Session, Browser) ->
    ok.

%% "Search the Knowledgebase"

