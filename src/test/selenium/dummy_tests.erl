%%% -*- mode: Erlang -*-
%%%----------------------------------------------------------------------------
%%% @author Kai Janson <kai.janson@rackspace.com
%%% [http://www.rackspace.com]
%%% @copyright 2012 Kai Janson / Rackspace
%%% @doc Selenium Test for Dummy Dummy
%%% @end
%%%----------------------------------------------------------------------------                                                                                
-module(dummy_tests).
-include_lib("eunit/include/eunit.hrl").
-export([dummy_page/2]).

suite_test_() ->
    [{setup,
      fun() ->
	      setup_session(B) end,
      fun close_session/1,
      api_tests(B)} || B <- cb_sel:get_browsers()].

api_tests(Browser) ->
    Tests = [
	     dummy_page
	    ],
    fun(X) ->
	    [{timeout, 120,
	      {lists:flatten(io_lib:format("~s with ~s", [T, Browser])), fun() ->
										 ?MODULE:T(Browser,X) end}
	     } || T <- Tests]
    end.

setup() ->
    ok.

setup_session(Browser) ->
    {ok, Session} = webdriver_remote:session(cb_sel:get_host(),
					     cb_sel:get_port(),
					     [
					      {browserName, Browser},
					      {javascriptEnabled, true},
					      {version, <<"">>},
					      {platform, 'ANY'}
					     ]
					    ),
    Session.

close_session(Session) ->
    {ok, no_content} = webdriver_remote:quit(Session).

%% ---- Your Testing Code Starts Here -----

%%-----------------------------------
%% @doc dummy_page
%%-----------------------------------
dummy_page(_Session, Browser) ->
    ok.
