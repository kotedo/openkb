%%% -*- mode: Erlang -*-
%%%----------------------------------------------------------------------------
%%% @author Kai Janson <kai.janson@rackspace.com
%%% [http://www.rackspace.com]
%%% @copyright 2012 Kai Janson / Rackspace
%%% @doc Selenium Test for dummy 1
%%% @end
%%%----------------------------------------------------------------------------                                                                                
-module(dummy_tests1).
-include_lib("eunit/include/eunit.hrl").
-export([test_the_dummy/2]).
-export([test_for_results/2]).

suite_test_() ->
    [{setup,
      fun() ->
	      setup_session(B) end,
      fun close_session/1,
      api_tests(B)} || B <- cb_sel:get_browsers()].

api_tests(Browser) ->
    Tests = [
	     test_the_dummy,
	     test_for_results
	    ],
    fun(X) ->
	    [{timeout, 120,
	      {lists:flatten(io_lib:format("~s with ~s", [T, Browser])), fun() ->
										 ?MODULE:T(Browser,X) end}
	     } || T <- Tests]
    end.

setup() ->
    ok.

setup_session(Browser) ->
    {ok, Session} = webdriver_remote:session(cb_sel:get_host(),
					     cb_sel:get_port(),
					     [
					      {browserName, Browser},
					      {javascriptEnabled, true},
					      {version, <<"">>},
					      {platform, 'ANY'}
					     ]
					    ),
    Session.

close_session(Session) ->
    {ok, no_content} = webdriver_remote:quit(Session).

%% ---- Your Testing Code Starts Here -----

before_(_) ->
    error_logger:error_msg("So much fun!",[]).

%%-----------------------------------
%% @doc test_the_dummy
%%-----------------------------------
test_the_dummy(_Browser, Session) ->
    {ok, no_content} = webdriver_remote:get(Session, "http://localhost:8001/results"),
    {ok, [Id]} = webdriver_remote:find_elements(Session, xpath, "//h2"),
    ?assertEqual({ok, <<"Results:">>}, webdriver_remote:text(Session, Id)).

dummy(_Browser, Session) ->
    {ok, [Table]} = webdriver_remote:find_elements(Session, xpath, "//tr//td"),
    ?assertEqual({ok, <<"Line 1">>}, webdriver_remote:text(Session, Table)),

    {ok, [Label]} = webdriver_remote:find_elements(Session, id, "user_shit_label"),
    ?assertEqual({ok, <<"User Shit">>}, webdriver_remote:text(Session, Label)),

    {ok, [TextInput]} = webdriver_remote:find_elements(Session, id, "user_shit"),
    ?assertEqual({ok, <<"nothing">>}, webdriver_remote:value(Session, TextInput)),

    ok.

test_for_results(_Browser, Session) ->
    webdriver_remote:screenshot(Session),
    {ok, no_content} = webdriver_remote:get(Session, "http://localhost:8001/results"),
    {ok, [Id]} = webdriver_remote:find_elements(Session, xpath, "//h2"),
    ?assertEqual({ok, <<"Results:">>}, webdriver_remote:text(Session, Id)).
