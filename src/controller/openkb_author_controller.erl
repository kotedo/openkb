-module(openkb_author_controller, [Req, SessionID]).
-compile(export_all).

index('GET', []) ->
    Authors = boss_db:find(author, []),
    {ok, [{authors, Authors}]}.

edit('GET', [Id]) ->
    Author = boss_db:find(Id),
    Roles = boss_db:find(role, []),
    {ok, [{author, Author}, {roles, Roles}]};
edit('POST', [Id]) ->
    Old = boss_db:find(Id),
    New = Old:set([
		   {sso, Req:post_param("sso")},
		   {first, Req:post_param("firstname")},
		   {last, Req:post_param("lastname")},
		   {email, Req:post_param("email")},
		   {role_id, Req:post_param("role_id")},
		   {updated_on, erlang:now()}
		  ]),
    case New:save() of
	{ok, UpdatedAuthor} ->
	    boss_flash:add(SessionID, success, "", "Successfully updated"),
	    {redirect, [{action, "index"}]};
	{error, Reason} ->
	    Reason
    end.

create('GET', []) ->
    Roles = boss_db:find(role, []),
    {ok, [{roles, Roles}]};
create('POST', []) ->
    %% Id, Sso, First, Last, Email, Active, RoleId, CreatedOn, UpdatedOn
    SSO = Req:post_param("sso"),
    First = Req:post_param("firstname"),
    Last = Req:post_param("lastname"),
    Email = Req:post_param("email"),
    RoleId = Req:post_param("role_id"),
    CreatedOn = now(),
    UpdatedOn = CreatedOn,
    
    case (author:new(id, SSO, First, Last, Email, true, RoleId, CreatedOn, UpdatedOn)):save() of
	{ok, NewAuthorData} ->
	    boss_flash:add(SessionID, success, "", string:concat("Author '", [First, " ", Last, "' created."])),
	    {redirect, [{action, "create"}]};
	{error, Reason} ->
	    Reason
    end.
