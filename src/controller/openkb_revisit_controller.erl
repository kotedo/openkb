-module(openkb_revisit_controller, [Req, SessionID]).
-compile(export_all).

%% --------------------------------------------------
%% Created: 04/02/2013 11:14:23 CDT
%% Author : Kai Janson (kai.janson@rackspace.com)
%% Purpose: List all queued revisitations of articles
%% --------------------------------------------------
index('GET', []) ->
    Revisits = boss_db:find(revisit, []),
    {ok, [{revisits, Revisits}]}.

mine('GET', ["author-" ++ _ = AuthorId]) ->
    Revisits = boss_db:find(revisits, [{author_id, AuthorId}]),
    {ok, [{revisits, Revisits}]}.

%% article, [Id, Title, Content, CategoryId, Tags, AuthorId, ArticleRatingId, CreatedOn, UpdatedOn, Status]

%% Edit the article and make sure it gets bumped when done
revisit('GET', ["article-" ++ _ = ArticleId]) ->
    Article = boss_db:find(ArticleId),
    {ok, [{article, Article}]};
revisit('PUT', [ArticleId]) ->
    OldArticle = boss_db:find(ArticleId),
    Title  = Req:post_param("title"),
    Content  = Req:post_param("content"),
    CategoryId  = Req:post_param("category_id"),
    Tags  = Req:post_param("tags"),

    RevisitedArticle = OldArticle:set([
				       {title, Title},
				       {content, Content},
				       {category_id, CategoryId},
				       {tags, Tags},
				       {updated_on, erlang:now()},
				       {revisit_on, now()}
				      ]),
    case RevisitedArticle:save() of
	{ok, SavedArticle} ->
	    {redirect, [{action, "index"}]};
	{error, Reason} ->
	    {ok, [{error, Reason}, {article, RevisitedArticle}]}
    end.
