-module(openkb_security_controller, [Req, SessionID]).
-compile(export_all).
-default_action(index).
-include_lib("eldap/include/eldap.hrl").

lang_(_) ->
    boss_session:get_session_data(SessionID, ui_language).

pick([]) ->
    [];
pick({_ReturnValue, Value}) ->
    Value;
pick(Anything) ->
    Anything.

index('GET', []) ->
    %% Get the login page
    ok;
index('POST', []) ->
    %% Create the session
    check_required_apps(),
    {ok, Hostname} = application:get_env(openkb, rack_ldap_host),
    {ok, Ssl} = application:get_env(openkb, rack_ldap_ssl),
    {ok, Port} = application:get_env(openkb, rack_ldap_port),
    
    Username = string:to_lower(Req:post_param("sso_username")),
    LdapSearchString = lists:flatten(io_lib:format("cn=~s, ou=users, o=rackspace", [Username])),
    LdapPassword = Req:post_param("sso_password"),
    {ok, Pid} = eldap:open( Hostname, [{ssl, Ssl},{port, Port}]),    
    
    case eldap:simple_bind(Pid, LdapSearchString, LdapPassword) of
	ok ->
	    %% Authentication was successful
	    %% Now we need to get some more information from LDAP
	    LDAPInfo = search(Pid, Username),
	    eldap:close(Pid),
	    Fullname = get("displayName", LDAPInfo),
	    Photo = get("photo", LDAPInfo),
	    save_photo(Photo, Username),
	    Email = get("mail", LDAPInfo),
	    set_userlevel(Username),
	    boss_session:set_session_data(SessionID, fullname, Fullname),
	    boss_session:set_session_data(SessionID, username, Username),
	    boss_session:set_session_data(SessionID, email, Email),
	    boss_session:set_session_data(SessionID, ui_language, "en"),
	    Msg = case boss_session:get_session_data(SessionID, user_level) of
		      "Racker Contributor" ->
			  lists:flatten(io_lib:format("Welcome ~s, you're logged in as an administrator", [Fullname]));
		      "Racker Contributor" ->
			  lists:flatten(io_lib:format("Welcome ~s, you're logged in as an contributor", [Fullname]));
		      "Racker Admin" ->
			  lists:flatten(io_lib:format("Welcome ~s, you're logged in as an administrator", [Fullname]));
		      "Racker" ->
			  lists:flatten(io_lib:format("Welcome Racker ~s, you have successfully logged in.", [Fullname]));
		      SomeoneElse ->
			  lists:flatten(io_lib:format("Welcome ~s, you have successfully logged in.", [SomeoneElse]))
		  end,
	    boss_flash:add(SessionID, success, "", Msg),
	    {redirect, [{controller, "main"}, {action, "index"}]};
	{error, invalidCredentials} ->
	    eldap:close(Pid),
	    boss_flash:add(SessionID, error, "", "Incorrect SSO username or SSO password."),
	    {redirect, [{action, "index"}]};
	{error, OtherReason} ->
	    boss_flash:add(SessionID, error, "Something unexpected happened: ", OtherReason),
	    eldap:close(Pid),
	    OtherReason,
	    {redirect, [{action, "index"}]}
    end;

index('DELETE', [Id]) ->
    %% Destroy the session
    boss_session:delete_session(SessionID),
    {redirect, [{action, "index"}]}.

logout('GET', []) ->
    boss_session:delete_session(SessionID),
    {redirect, [{controller, "main"}, {action, "index"}]}.

check_required_apps() ->
    case proplists:is_defined(public_key, application:which_applications()) of
	false ->
	    application:start(public_key);
	true ->
	    ok
    end,
    
    case proplists:is_defined(ssl, application:which_applications()) of
	false ->
	    application:start(ssl);
	true ->
	    ok
    end.

search(Pid, Username) when is_pid(Pid) ->
    Base = {base, pick(application:get_env(kb, rack_ldap_base))},
    Scope = {scope, pick(eldap:wholeSubtree())},
    Filter = {filter, eldap:equalityMatch("uid", Username)},
    Search = [Base, Scope, Filter],
    {ok, Result} = eldap:search(Pid, Search),
    hd(Result#eldap_search_result.entries).

get(What, Data) ->
    Result = proplists:get_value(What, Data#eldap_entry.attributes),
    Result.

save_photo(Photo, Username) ->
    Filename = lists:flatten(io_lib:format("priv/static/img/~s.jpg", [string:to_lower(Username)])),
    {ok, File} = file:open(Filename, [write]),
    file:write(File, Photo),
    file:close(File).

set_userlevel(Username) ->
    case boss_db:find(author, [{sso, Username},{active, true}]) of
	[] ->
	    boss_session:set_session_data(SessionID, user_level, "Racker");
	[Admin] ->
	    Role = Admin:role(),
	    boss_session:set_session_data(SessionID, user_level, binary_to_list(Role:name()))
    end.
