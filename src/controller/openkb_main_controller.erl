-module(openkb_main_controller, [Req, SessionID]).
-compile(export_all).
-default_action(index).

index('GET', []) ->
    Authors = boss_db:find(author, []),
    Categories = boss_db:find(category, []),
    Articles = boss_db:find(article, []),
    {ok, [{articles, Articles}, {categories, Categories}, {authors, Authors}]};
index('GET', [Id]) ->
    ok;
index('POST', []) ->
    error_logger:info_msg("TinyMCE: ~p~n", [Req:post_param("tinymce")]),
    ok;
index('PUT', [Id]) ->
    ok;
index('DELETE', [Id]) ->
    ok.
