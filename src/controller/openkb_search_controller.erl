-module(openkb_search_controller, [Req, SessionID]).
-compile(export_all).

index('GET', []) ->
    Categories = boss_db:find(category, []),
    Authors = boss_db:find(author, []),
    error_logger:info_msg("standard_search w/o arguments~n"),
    {ok, [{categories, Categories}, {authors, Authors}]};
index('GET', [SearchTerm]) ->
    error_logger:info_msg("standard_search w/ argument: ~p~n", [SearchTerm]),
    ok;
index('POST', []) ->
    SearchResult = case Req:post_param("searchtype") of
		       "simple" ->
			   search:simple(Req:post_param("searchstring"));
		       "advanced" ->
			   ok;
		       undefined  ->
			   [{error, "Ooops, the form doesn't have a searchtype"}]
		   end,
    error_logger:info_msg("SearchResult: ~p~n", [SearchResult]),
    {ok, [{results, SearchResult}]}.

oldsearch('POST', []) ->
    ArticlesByAuthor = case Req:post_param("by_author") of
			   "true" ->
			       boss_db:find(article, [{author_id, Req:post_param("searchAuthor")}]);
			   _ ->
			       []
		       end,
    ArticlesByCategory = case Req:post_param("by_category") of
			     "true" ->
				 boss_db:find(article, [{category_id, Req:post_param("searchCategory")}]);
			     _ ->
				 []
			 end,
    ArticlesByTag = case Req:post_param("by_tag") of
			"true" ->
			    boss_db:find(article, [{tags, matches, Req:post_param("searchTag"), [caseless]}]);
			_ ->
			    []
		    end,
    ArticlesByTitles = case string:strip(Req:post_param("search")) of
			   [] ->
			       error_logger:info_msg("No text in search text field~n"),
			       [];
			   SearchText ->
			       case boss_db:find(article, [{title, matches, SearchText, [caseless]}]) of
				   [] ->
				       error_logger:info_msg("No data found for ~p by title search only, retrying with full text body search~n", [SearchText]),
				       Data = boss_db:find(article, [{content, matches, SearchText, [caseless]}]),
				       error_logger:info_msg("Data found now: ~p~n", [Data]),
				       Data;
				   Articles ->
				       error_logger:info_msg("Found articles for ~p by title search only: ~p~n", [SearchText, Articles]),
				       Articles
			       end
		       end,
    {ok, [{articles_by_author, ArticlesByAuthor},
	  {by_categories, ArticlesByCategory},
	  {by_tags, ArticlesByTag},
	  {by_titles, ArticlesByTitles},
	  {dummy, "Yes, I was called"}
	 ]}.

view('GET', []) ->
    ok;
view('GET', [Id]) ->
    Article = boss_db:find(Id), 
    {ok, [{article, Article}]}.

tagsearch('GET', []) ->
    Tag = Req:query_param("tag"),
    error_logger:info_msg("Tag to search for (no param): ~p~n", [Tag]),
    {ok, [{tag, Tag}]};
tagsearch('GET', [Tag]) ->
    error_logger:info_msg("Tag to search for (w/ param): ~p~n", [Tag]),
    ArticlesByTag = boss_db:find(tag_article, [{tag, Tag}]),
    {ok, [{criteria, Tag}, {articles, ArticlesByTag}]}.

categorysearch('GET', []) ->
    Category = Req:query_param("category"),
    error_logger:info_msg("Category to search for: ~p~n", [Category]),
    {ok, [{category, Category}]};
categorysearch('GET', [Category]) ->
    error_logger:info_msg("Tag to search for: ~p~n", [Category]),
    {ok, [{category, Category}]}.

authorsearch('GET', []) ->
    AuthorId = Req:query_param("author_id"),
    Author = boss_db:find(AuthorId),
    error_logger:info_msg("Author to search for: ~p~n", [AuthorId]),
    Articles = boss_db:find(article, [{author_id, AuthorId}]),
    {ok, [{author, Author}, {articles, Articles}]};
authorsearch('GET', [Author]) ->
    error_logger:info_msg("Tag to search for: ~p~n", [Author]),
    {ok, [{author, Author}]}.
