-module(openkb_article_controller, [Req, SessionID]).
-compile(export_all).
-include("statuses.hrl").

-define(MAIN_PATH, [{controller, "main"}, {action, "index"}]).
-define(ARTICLES_PATH, [{controller, "article"}, {action, "index"}]).
-define(CREATE_PATH, [{controller, "article"}, {action, "create"}]).
-define(EDIT_PATH, [{controller, "article"}, {action, "edit"}]).

%% before_(_) ->
    %% boss_db:find_first(author, [{sso, "kai.janson"}]).

index('GET', []) ->
    %% Index
    Articles = boss_db:find(article, []),
    {ok, [{articles, Articles}]};
index(GET, [Id]) ->
    %% Show
    {ok, [{}]}.

view('GET', ["article-" ++ _ = Id]) ->
    Article = boss_db:find(Id),
    {ok, [{article, Article}]}.

create('GET', []) ->
    %% Create; renders create page
    Categories = boss_db:find(category, []),
    {ok, [{categories, Categories}]};
create('POST', []) ->
    %% Create: saves the create page
    Security = boss_db:find_first(author, [{sso, "kai.janson"}]),
    Title = Req:post_param("title"),
    Content = Req:post_param("content"),
    CategoryId = Req:post_param("category"),
    Tags = Req:post_param("tags"),
    AuthorId = Security:id(),
    RatingId = [],

    %% [Id, Title, Content, CategoryId, Tags, AuthorId, RatingId, CreatedOn, UpdatedOn]).
    Article = article:new(id, Title, Content, CategoryId, Tags, AuthorId, RatingId, utils:timestamp(), utils:timestamp(), ?ACTIVE),
    case Article:save() of
	{ok, NewArticle} ->
	    boss_flash:add(SessionID, success, "", "Article saved."),
	    {redirect, ?MAIN_PATH};
	{error, Reason} ->
	    boss_flash:add(SessionID, error, "An error occurred:", Reason),
	    {redirect, ?CREATE_PATH}
    end.

edit('GET', [Id]) ->
    %% Edit: renders edit page
    Categories = boss_db:find(category, [], [{order_by, name}]),
    Article = boss_db:find(Id),
    {ok, [{article, Article}, {categories, Categories}]};
edit('POST', [Id]) ->
    %% Saves the edited record
    OldArticle = boss_db:find(Id),
    NewTags = Req:post_param("tags"),
    Title = Req:post_param("title"),
    Content = Req:post_param("content"),
    UpdatedArticle = OldArticle:set([
				     {tags, NewTags},
				     {title, Title},
				     {content, Content},
				     {updated_on, utils:timestamp()}
				    ]),
    case UpdatedArticle:save() of
	{ok, WrittenArticle} ->
	    OldTags = boss_db:find(tag_article, [{article_id, Id}]),
	    error_logger:info_msg("TAGS to be DELETED: ~p~n", [OldTags]),
	    error_logger:info_msg("TAGS to be CREATED: ~p~n", [string:tokens(NewTags,",")]),
	    [boss_db:delete(Tag:id()) || Tag <- OldTags],
	    [(tag_article:new(id,WrittenArticle:id(),WrittenArticle:author_id(),string:to_lower(string:strip(Tag, both)),Title)):save() || Tag <- string:tokens(NewTags,",")],
	    boss_flash:add(SessionID, success, "", "Article updated."),
	    {redirect, ?ARTICLES_PATH};
	{error, Reason} ->
	    boss_flash:add(SessionID, error, "Something went wrong", Reason),
	    {redirect, ?EDIT_PATH}
    end.

obsolete('GET', [ArticleId]) ->
    %% [Id, Title, ArticleId, Reason]
    Article = boss_db:find_first(article, [{id, ArticleId}]),
    NewArticle = Article:set([{status, ?OBSOLETE}, {updated_on, utils:timestamp()}]),
    NewArticle:save(),
    Reason = "It's outdated.",
    Obsolete = obsolete:new(id, Article:title(), ArticleId, Reason),
    Obsolete:save(),
    boss_flash:add(SessionID, success, "", "Article marked as obsolete"),
    {redirect, ?ARTICLES_PATH}.

delete('GET', ["article-" ++ _ = Id], Security) ->
    %% Deletes a record
    boss_db:delete(Id),
    {redirect, ?ARTICLES_PATH}.

templates('GET', []) ->
    Templates = "['Handover-Ticket', '/static/templates/handover.html'],
		 ['Intensive Managed Linux Spheres of Support', '/static/templates/intemanlinsos.html'],
		 ['RackConnect', '/static/templates/rackconnect.html'],
		 ['Rackspace Corporate Customer Runbook', '/static/templates/corporaterunbook.html'],
		 ['Rackspace Customer Runbook','/static/templates/customerrunbook.html'],
		 ['Rackspace Generic', '/static/templates/generic.html'],
		 ['Rackspace Goal', '/static/templates/goal.html'],
		 ['Problem + Solution','/static/templates/problemsolution.html'],
		 ['Rackspace System', '/static/templates/rackspacesystem.html'],
		 ['cas/Audit', '/static/templates/casaudit.html'],
		 ['eBay Maintenance', '/static/templates/ebaymaintenance.html']",
    {output, "var tinyMCETemplateList = new Array(" ++ Templates ++ ");"}.

imglist('GET', []) ->
    Res = "['glyphicons-halflings-white.png', '/static/img/glyphicons-halflings-white.png'],
	   ['glyphicons-halflings.png', '/static/img/glyphicons-halflings.png'],
	   ['openkb_logo.png', '/static/img/openkb_logo.png'],
	   ['openkb_logo1.png', '/static/img/openkb_logo1.png']",
    {output, "var tinyMCEImageList = new Array(" ++ Res ++ ");"}.

browse('GET', []) ->
    %% Stuff here
    Data = boss_db:find(article, []),
    {ok, [{data, Data}]}.


related_by(tags, Tags, Avoid) ->
    %% Articles = lists:map(fun(Tag) ->
    %% 				 boss_db:find(tag_article, [{tag, Tag},{article_id,'not_equals',Avoid}])
    %% 			 end, Tags),
    
    Articles = lists:foldl(fun(Tag,Acc) ->
    				   R = boss_db:find(tag_article, [{tag, Tag},{article_id,'not_equals',Avoid}]),
    				   case R of
    				       [] ->
    					   Acc;
    				       [Data] ->
    					   [Data|Acc]
    				   end
    			   end, [], Tags),
    lager:info("Articles: ~p~n", [Articles]),
    lists:usort(fun(X,Y) -> X:tag() =/= Y:tag() end, Articles );
    %% Articles;
related_by(title, Tags, Avoid) ->
    ok;
related_by(content, Tags, Avoid) ->
    ok.

show('GET', [Id]) ->
    Article = boss_db:find(Id),
    Tags = utils:make_tags(Article:tags()),
    RelatedArticles = related_by(tags, Tags, Id),

    case length(Tags) of
	0 ->
	    {ok, [{article, Article}, {related_articles, RelatedArticles}]};
        1 ->
	    Tag1 = lists:nth(1, Tags),
	    {ok, [{article, Article}, {related_articles, RelatedArticles}, {tag1, Tag1}]};
	2 ->
	    Tag1 = lists:nth(1, Tags),
	    Tag2 = lists:nth(2, Tags),
	    {ok, [{article, Article}, {related_articles, RelatedArticles}, {tag1, Tag1}, {tag2, Tag2}]};
	3 ->
	    Tag1 = lists:nth(1, Tags),
	    Tag2 = lists:nth(2, Tags),
	    Tag3 = lists:nth(3, Tags),
	    {ok, [{article, Article}, {related_articles, RelatedArticles}, {tag1, Tag1}, {tag2, Tag2}, {tag3, Tag3}]};
	4 ->
	    Tag1 = lists:nth(1, Tags),
	    Tag2 = lists:nth(2, Tags),
	    Tag3 = lists:nth(3, Tags),
	    Tag4 = lists:nth(4, Tags),
	    {ok, [{article, Article}, {related_articles, RelatedArticles}, {tag1, Tag1}, {tag2, Tag2}, {tag3, Tag3}, {tag4, Tag4}]};
	5 ->
	    Tag1 = lists:nth(1, Tags),
	    Tag2 = lists:nth(2, Tags),
	    Tag3 = lists:nth(3, Tags),
	    Tag4 = lists:nth(4, Tags),
	    Tag5 = lists:nth(5, Tags),
	    {ok, [{article, Article}, {related_articles, RelatedArticles}, {tag1, Tag1}, {tag2, Tag2}, {tag3, Tag3}, {tag4, Tag4}, {tag5, Tag5}]};
	6 ->
	    Tag1 = lists:nth(1, Tags),
	    Tag2 = lists:nth(2, Tags),
	    Tag3 = lists:nth(3, Tags),
	    Tag4 = lists:nth(4, Tags),
	    Tag5 = lists:nth(5, Tags),
	    Tag6 = lists:nth(6, Tags),
	    {ok, [{article, Article}, {related_articles, RelatedArticles}, {tag1, Tag1}, {tag2, Tag2}, {tag3, Tag3}, {tag4, Tag4}, {tag5, Tag5}, {tag6, Tag6}]};
	_ ->
	    Tag1 = lists:nth(1, Tags),
	    Tag2 = lists:nth(2, Tags),
	    Tag3 = lists:nth(3, Tags),
	    Tag4 = lists:nth(4, Tags),
	    Tag5 = lists:nth(5, Tags),
	    {ok, [{article, Article}, {related_articles, RelatedArticles},
		  {tag1, Tag1}, {tag2, Tag2}, {tag3, Tag3}, {tag4, Tag4}, {tag5, Tag5},
		  {tag6, "More tags to display..."}]}
    end.

myexternallist('GET', []) ->
    List = [["Logo 1", "logo.jpg"],["Logo 2", "logo2.jpg"]],
    {output, List, [{'Content-Type', "text/javascript"},{'pragma', "no-cache"},{'expires', 0}]}.

