-module(openkb_category_controller, [Req, SessionID]).
-compile(export_all).

index('GET', []) ->
    Categories = boss_db:find(category, []),
    {ok, [{categories, Categories}]}.

edit('GET', [Id]) ->
    Category = boss_db:find(Id),
    {ok, [{category, Category}]};
edit('POST', [Id]) ->
    Old = boss_db:find(Id),
    New = Old:set([{name, Req:post_param("name")}]),
    case New:save() of
	{ok, SavedCategory} ->
	    ok;
	{error, Reason} ->
	    Reason
    end.

create('GET', []) ->
    ok;
create('POST', []) ->
    Name = Req:post_param("name"),
    
    case (category:new(id, Name)):save() of
	{ok, SavedCategory} ->
	    boss_flash:add(SessionID, success, "", string:concat("Category '", [Name, "' created."])),
	    {redirect, [{action, "create"}]};
	{error, Reason} ->
	    boss_flash:add(SessionID, error, "", string:concat("Category '", [Name, "' already defined."])),
	    {redirect, [{action, "create"}], [{errors, Reason}]}
    end.

delete('GET', ["category-" ++ _ = Id]) ->
    Name = (boss_db:find(Id)):name(),
    boss_db:delete(Id),
    boss_flash:add(SessionID, success, "", string:concat("Category '", [Name, "' successfully removed"])),
    {redirect, [{action, "index"}]}.
