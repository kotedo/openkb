-module(bug_bug_controller, [Req, SessionID]).
-compile(export_all).

index('GET', []) ->
    %% Index
    Bugs = boss_db:find(bug, []),
    {ok, [{bug, Bugs}]}.

show(GET, ["bug-" ++ _ = Id]) ->
    %% Show
    Bug = boss_db:find(Id),
    {ok, [{bug, Bug}]}.

create('GET', []) ->
    %% Create; renders create page
    {ok, []};
create('GET', [Title]) ->
    TheTitle = case boss_db:find(title, [{key, Title}]) of
		   [TitleFromDb] ->
		       TitleFromDb:name();
		   [] ->
		       case Title of
			   [] ->
			       "Report a problem";
			   _ ->
			       string:concat("Report a problem with ", Title)
		       end
	       end,
    {ok, [{title, TheTitle}]};
create('POST', []) ->
    %% Create: saves the create page
    
    %% Add fields here
    NewBug = bug:new(id, Req:post_param("subject"), Req:post_param("body")),
    
    case NewBug:save() of
	{ok, SavedBug} ->
	    boss_flash:add(SessionID, success, "", "Bug successfully created."),
	    {redirect, [{action, "index"}]};
	{error, Errors} ->
	    boss_flash:add(SessionID, error, "", "Bug failed to be created."),
	    {redirect, [{action, "create"}, {errors, Errors}]}
    end;
create('POST', [Title]) ->
    %% Create: saves the create page
    
    %% Add fields here
    NewBug = bug:new(id, Req:post_param("subject"), Req:post_param("body")),
    
    case NewBug:save() of
	{ok, SavedBug} ->
	    boss_flash:add(SessionID, success, "", "Bug successfully created."),
	    {redirect, [{action, "index"}]};
	{error, Errors} ->
	    boss_flash:add(SessionID, error, "", "Bug failed to be created."),
	    {redirect, [{action, "create"}, {errors, Errors}]}
    end.



edit('GET', ["bug-" ++ _ = Id]) ->
    %% Edit: renders edit page
    Bug = boss_db:find(Id),
    {ok, [{bug, Bug}]};
edit('POST', ["bug-" ++ _ = Id]) ->
    %% Saves the edited record
    OldBug = boss_db:find(Id),
    NewBug = OldBug:set([{subject, Req:post_param("subject")},{body, Req:post_param("body")}]),
    
    case NewBug:save() of
	{ok, UpdatedBug} ->
	    boss_flash:add(SessionID, success, "", "Bug successfully updated."),
	    {redirect, [{action, "index"}]};
	{error, Errors} ->
	    boss_flash:add(SessionID, error, "", "Bug failed to update."),
	    {redirect, [{action, "edit"}, {errors, Errors}]}
    end.

delete('GET', ["bug-" ++ _ = Id]) ->
    %% Deletes a record
    boss_db:delete(Id),
    {redirect, [{action, "index"}]}.

