-module(bug_tests).
-compile(export_all).

-include_lib("eunit/include/eunit.hrl").


start_test() ->
    io:format("Running EUnit tests ~p~n", [?assertMatch(true, 1 =:= 1)]).

dummy_test() ->
    ?assertMatch(true, 1 == 1).
