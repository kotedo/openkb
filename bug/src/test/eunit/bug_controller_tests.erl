-module(bug_controller_tests).
-compile(export_all).

-include_lib("eunit/include/eunit.hrl").

setup() ->
    error_logger:error_msg("~p.~n", ["Setup"]).

tear_down(A) ->
    io:format("Teardown; A: ~p~n", [A]).

create_test() ->
    Answer = bug_bug_controller:create('GET', []),
    error_logger:error_msg("Answer: ~p~n", [Answer]),
    ?assertMatch(true, Answer).
